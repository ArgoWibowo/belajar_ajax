<!DOCTYPE HTML>
<html>
<head>
	<script src="./jquery.min.js"></script>
	<script type="text/javascript">
		$(document).ready(function(){
			$("#btnLoad1").click(function(){
				$("#div1").load("https://jsonplaceholder.typicode.com/posts");
			});

			//skema untuk post dan get
			//$.get("url",function);
			//$.post("url",data,function);

			$("#btnLoad2").click(function(){
				$.get("https://jsonplaceholder.typicode.com/posts",function(data,status){
					var output = "<table><thead><tr><th>User ID</th><th>ID</th>"+
					"<th>Title</th><th>Body</th></tr><tbody>";
					for (var i in data) {
						output += "<tr><td>" +
						data[i].userId +
						"</td><td>" +
						data[i].id +
						"</td><td>" +
						data[i].title +
						"</td><td>" +
						data[i].body +
						"</td></tr>";
					};
					output += "</tbody></table>";
					$("#div2").html(output);
				});
			});

			$("#btnLoad3").click(function(){
				$.post("https://jsonplaceholder.typicode.com/posts",
				{
					title: 'foo',
					body: 'bar',
					userId: 1
				},
				function(data,status){
					$("#div3").html(data.title);
					console.log(data);
					alert(data.title);
				});
			});

			$("#btnLoad4").click(function(){
				$.getJSON('./twitter-proxy.php?url='+encodeURIComponent('statuses/user_timeline.json?screen_name=BPPTKG&count=7'), function(data){
			      console.log(data);
			      var output = "<table><thead><tr><th>Text Tweet</th></tr><tbody>";
					for (var i in data) {
						output += "<tr><td>" +
						data[i].text +
						"</td></tr>";
					};
					output += "</tbody></table>";
					$("#div4").html(output);
			    });
			});
		});
	</script>
	<body>
		<button id="btnLoad1">Load Data API</button>
		<div id="div1"><h2>Data Loaded From API</h2></div>

		<button id="btnLoad2">Get Data API</button>
		<div id="div2"><h2>Data Get From API</h2></div>

		<button id="btnLoad3">Post Data API</button>
		<div id="div3"><h2>Data Post Result From API</h2></div>

		<button id="btnLoad4">Get Data Twitter API</button>
		<div id="div4"><h2>Data Timeline Twitter</h2></div>
	</body>
</head>
</html>